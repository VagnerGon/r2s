#ifndef __R2S_HEADER__

	#define  __R2S_HEADER__

	#include <cuda_runtime.h>
	#include <opencv2//core//core.hpp>

	extern "C" int kR2S_int(unsigned char* imagem, int tamanho);
	extern "C" float kR2S_float(unsigned char* imagem, int tamanho);
	extern "C" double kR2S_double(unsigned char* imagem, int tamanho);
	extern "C" unsigned char kR2S_uchar(unsigned char* imagem, int tamanho);
			
	template<class T>
	class R2S {

		int tamanho;
		int altura;
		int largura;

		protected:

			static bool verificaHardwareCompativel(cudaError_t* error_id);
			cudaError_t erro;

		public:	

			unsigned char* imagemOriginal;
			
			int result;

			explicit R2S(IplImage* imagemInput) ;
			
			R2S(unsigned char* imagemInput, int altura, int largura);

			T processa();

			int base_processa();
	};

#endif
