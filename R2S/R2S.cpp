#include <opencv2//core//core.hpp>
#include <opencv2//highgui//highgui.hpp>
#include <cuda_runtime.h>
#include <iostream>
#include <ctime>

#include "R2S.h"

template <class T>
R2S<T>::R2S(IplImage* imagemInput) : imagemOriginal((unsigned char*) imagemInput->imageData) {
	altura = imagemInput->height;
	largura = imagemInput->widthStep;
	tamanho = altura * largura;
}

template <class T>
R2S<T>::R2S(unsigned char* imagemInput, int altura, int largura): 
	imagemOriginal(imagemInput), 
	altura(altura), largura(largura), tamanho(altura*largura)
	{	}

template <class T>
bool R2S<T>::verificaHardwareCompativel(cudaError_t* error_id){

		int deviceCount;
	
		*error_id = cudaGetDeviceCount(&deviceCount);
	
		if(*error_id != cudaSuccess || deviceCount < 1)		
			return false;
	
		return true;
	}

template <class T>
T R2S<T>::processa() {
	std::cout << "Type unsupported" << std::endl;
	return 0;
}

template <class T>
int R2S<T>::base_processa() {

	if(!verificaHardwareCompativel(&erro))
		return erro;

	return cudaSetDevice(0);
}

template <>
int R2S<int>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_int(imagemOriginal, tamanho);
}

template <>
float R2S<float>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_float(imagemOriginal, tamanho);
}

template <>
double R2S<double>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_double(imagemOriginal, tamanho);
}

template <>
unsigned char R2S<unsigned char>::processa() {
	
	int erro = base_processa();
	if(erro != cudaSuccess)
		return erro;

	return kR2S_uchar(imagemOriginal, tamanho);
}

int main(int argc, char* argv[]){

	clock_t start,finish;

	start = clock();

	const char* imagemEntrada = argc == 1 ? "img.jpg" : argv[1]; 

	IplImage *imagem = cvLoadImage(imagemEntrada, CV_LOAD_IMAGE_GRAYSCALE);

	R2S<unsigned char> r2s(imagem);

	//printaTempo(&start, "Carregada imagem");
	//unsigned char result = 
	std::cout << +r2s.processa() << std::endl;
	
	//r2s.result;

	//printaTempo(&start, "Terminado!");

}