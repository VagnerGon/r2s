#include <cuda.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "R2S.h"
#include <iostream>
#include <ctime>

#define CHK_ERROR if (erro != cudaSuccess) goto Error;				

//Retorna zero caso forem iguais

template <class E, class T>
__device__
T maior(E pixel1, E pixel2) {
	
	return pixel1 > pixel2 ? pixel1 : pixel2;//(pixel1+pixel2)/2;;
}

template <class E, class T>
__global__ void R2S_kernel(E* imagem, T* result, int tamanho) {
	
	unsigned int index = threadIdx.x + blockIdx.x * blockDim.x * 2;

	if(index > tamanho)
		return;		

	__shared__ T smem[1024];//TODO usar blocksize
	unsigned int sIndex = threadIdx.x;

	if(index+blockDim.x < tamanho)
		smem[sIndex] = maior<E, T>(imagem[index], imagem[index+blockDim.x]);
	else
		smem[sIndex] = imagem[index];

	__syncthreads();

	for(unsigned int cmp_pixel = blockDim.x/2; cmp_pixel > 0; cmp_pixel=cmp_pixel/2) {
		if(sIndex < cmp_pixel && index+sIndex+cmp_pixel < tamanho)
			smem[sIndex] = maior<E, T>(smem[sIndex],smem[sIndex+cmp_pixel]);
		__syncthreads();
	}

	if(threadIdx.x == 0)
		result[blockIdx.x] = smem[0];
}

static void printaTempo(clock_t* start, char* texto) {
	clock_t finish = clock() - *start;
	double interval = finish / (double)CLOCKS_PER_SEC; 
	std::cout << texto <<  std::endl << " - " << interval * 1000 << "ms" << std::endl;
	*start = clock();
}

template 
__global__ void R2S_kernel <unsigned char, int>(unsigned char* imagem, int* result, int tamanho);

template 
__global__ void R2S_kernel <int, int>(int* imagem, int* result, int tamanho);

template<class T>
T kR2S(unsigned char* imagem, int tamanho) {
	
	cudaDeviceProp deviceProp;
	cudaError_t erro;	
	unsigned char* d_image;
	T* d_result;
	bool singleBlock = false;

	clock_t start = clock();

	cudaEvent_t startCuda, stop;
	cudaEventCreate(&startCuda);
	cudaEventCreate(&stop);

	cudaEventRecord(startCuda);

	erro = cudaMalloc((void**)&d_image,sizeof(unsigned char)*tamanho); CHK_ERROR

	erro = cudaMemcpy(d_image,imagem,tamanho*sizeof(unsigned char),cudaMemcpyHostToDevice);	
	
	cudaGetDeviceProperties(&deviceProp, 0);

	int blockSize = deviceProp.maxThreadsPerBlock/2;
	int nBlocks = tamanho/blockSize + (tamanho%blockSize == 0 ? 0:1);
	
	if(nBlocks == 1) {
		nBlocks++;
		singleBlock = true;
	}

	nBlocks = nBlocks % 2 == 1 ? nBlocks/2+1 : nBlocks/2;

	erro = cudaMalloc((void**)&d_result,sizeof(T)*nBlocks); CHK_ERROR

	printaTempo(&start, "cudaMallocs");

	R2S_kernel<unsigned char, T> <<<nBlocks, blockSize>>>(d_image, d_result,tamanho);

	erro = cudaGetLastError(); CHK_ERROR
	
	//Espera por GPU terminar o trabalho
	erro = cudaDeviceSynchronize(); CHK_ERROR
	
	T* result = (T*) malloc(sizeof(T)*nBlocks);
	erro = cudaMemcpy(result, d_result, sizeof(T)*nBlocks,cudaMemcpyDeviceToHost); CHK_ERROR

	cudaFree(d_image);
	T* novaEntrada;

	while (!singleBlock) {
				
		cudaFree(d_result);

		int dimensaoNova = nBlocks;

		erro = cudaMalloc((void**)&novaEntrada,sizeof(T)*dimensaoNova); CHK_ERROR

		erro = cudaMemcpy(novaEntrada,result,dimensaoNova*sizeof(T),cudaMemcpyHostToDevice);	

		nBlocks = dimensaoNova/blockSize + (dimensaoNova%blockSize == 0 ? 0:1);
		
		if(nBlocks == 1) {
			nBlocks++;
			singleBlock = true;
		}
		
		nBlocks = nBlocks % 2 == 1 ? nBlocks/2+1 : nBlocks/2;

		erro = cudaMalloc((void**)&d_result,sizeof(T)*nBlocks); CHK_ERROR

		R2S_kernel<T, T><<<nBlocks, blockSize>>>(novaEntrada, d_result,dimensaoNova);

		erro = cudaGetLastError(); CHK_ERROR
	
		//Espera por GPU terminar o trabalho
		erro = cudaDeviceSynchronize(); CHK_ERROR		
		
		erro = cudaMemcpy(result, d_result, sizeof(T)*nBlocks,cudaMemcpyDeviceToHost); CHK_ERROR
		
		cudaFree(novaEntrada);
	} 
		
	cudaFree(d_image);		
	cudaFree(d_result);

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, startCuda, stop);

	std::cout << "cudaEventElapsedTime in milliseconds : " << milliseconds << std::endl;

	return result[0];
	
Error:	
	std::cerr << "Error on CUDA: " << cudaGetErrorString(erro);
	cudaFree(d_image);
	cudaFree(d_result);
	return -1;
}
 
extern "C" int kR2S_int(unsigned char* imagem, int tamanho) {
	return kR2S<int>(imagem, tamanho);
}
extern "C" float kR2S_float(unsigned char* imagem, int tamanho) {
	return kR2S<float>(imagem, tamanho);
}
extern "C" double kR2S_double(unsigned char* imagem, int tamanho) {
	return kR2S<double>(imagem, tamanho);
}
extern "C" unsigned char kR2S_uchar(unsigned char* imagem, int tamanho) {
	return kR2S<unsigned char>(imagem, tamanho);
}